var map = L.map('map')
    .setView([40.700536,-99.095825], 16)

var osmLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'FOOO',
    maxZoom: 25
}).addTo(map);



var libCoord = [[40.7003111, -99.0965101], [40.7003149, -99.0971806], [40.7008232, -99.0971726], [40.7008151, -99.0964323], [40.7007582, -99.096435], [40.7007521, -99.0964215], [40.7007378, -99.0964054], [40.7007216, -99.0964054], [40.7007094, -99.0964215], [40.7007134, -99.0964832], [40.7006972, -99.0964859], [40.7006951, -99.0964054], [40.700624, -99.0964081], [40.700626, -99.0965127], [40.7005752, -99.0965154], [40.7005752, -99.0964108], [40.7003739, -99.0964108], [40.7003739, -99.0965127], [40.7003111, -99.0965101]];

var a = new Building(libCoord);



a.onClick(function () {
    
    a.focusOn();
    
});


var img = L.imageOverlay('img/library_first_floor.png',
               a.polygon.getBounds());
var img2 = L.imageOverlay('img/library_second_floor.png',
               a.polygon.getBounds());

a.addFloor('First Floor', img, true);

a.addFloor('Second Floor', img2);

/*
var ff = L.layerGroup([img]);

L.control.layers({
    "First Floor": ff
}, null, {collapsed:false}).addTo(map);
*/
a.addTo(map);




map.on('zoomend', function(e) {

    if(e.target._zoom >= 20)
        map.removeLayer(osmLayer);
});


map.addControl(new FloorControl());



/*
L.imageOverlay(
    'https://dl.dropboxusercontent.com/u/15965953/Icons/library_floor1.png',
    [[40.70081, -99.09718], [40.70031, -99.09651]]).addTo(map);
*/
/*
var popup = L.popup()
  .setLatLng([40.70059, -99.09681])
  .setContent("words")
  marker.bindPopup("words") <-- this is throwing an error.
  // You can see errors by pressing F12 in chrome.
  .openOn(map);
*/

/*
// Create a marker object
var libraryMarker = L.marker([40.70055, -99.09679])

// have it open a popup when clocked
libraryMarker.bindPopup("words")

// Add it to the map
libraryMarker.addTo(map);



L.marker([40.70055, -99.09681]).addTo(map);


L.control.layers(null, { "Marker": "d" })
    .addTo(map);
    */
