var Building = (function (L) {
    
    function Building (coordinates) {
        
        this._floorLayerGroup = L.layerGroup();
            
        this.map = null;
        
        this.polygon = L.polygon(coordinates);
        //this.markers = LatLngBounds.contains( <LatLng> latlng );
        
        this.floors = {};
        
        this._floorControl = L.control.layers(null, null, {
            collapsed: false
        });
        
        this._defaultFloor = null;
        
        Object.seal(this);
    }


    Building.prototype = {
        
        focusOn: function () {
            
            if(!this._isAddedToMap())
                throw "No Map!";
                
            this.map.fitBounds(this.polygon.getBounds());
            
            this.map.addControl(this._floorControl);
            
            this._defaultFloor.addTo(this.map);
            
            /*
            if(this.floors.length != 0)
                this.floors[0].imageOverlay.addTo(this.map)
            */
        },
        
        addFloor: function (name, imgOL, addAsDefault) {
            
            // If the floor does not exist
            if(!this._floorExists(name)) {
                
                this.floors[name] = L.layerGroup();
                
                this._floorControl.addBaseLayer(this.floors[name], name);
                
                if(addAsDefault)
                    this._defaultFloor = this.floors[name];
            }
            
            this.floors[name].addLayer(imgOL);
        },
        
        addTo: function (map) {
            this.map = map;
            this.polygon.addTo(this.map);
            return this;
        },
        
        _floorExists: function (name) {
            return this.floors[name] != undefined;
        },
        
        _isAddedToMap: function () {
            return this.map != null;
        },
        
        onClick: function (delegate) {
            
            this.polygon.on('click', delegate);
        }
        
    };
    
    return Building;
    
})(L);

