describe('Building module', function() {
    
    var building, buildingPolyline, map;
    
    beforeEach(function(){
        
        // Spy on map
        map = L.map(document.createElement('div'));
        spyOn(map, 'fitBounds');
        spyOn(map, 'addControl');
        
        spyOn(L, 'polygon').andCallThrough();
        spyOn(L.control, 'layers').andCallThrough();
        
        buildingPolyline = [
            [0, 0],
            [0, 1],
            [1, 1],
            [1, 0]
        ];
        
        building = new Building(buildingPolyline);
    });
    
    describe('when instantiated', function () {
        
        it('should create a new polygon based upon the building coordinates', function () {
            expect(L.polygon).toHaveBeenCalledWith(buildingPolyline);
        });
        
        it('should set the polygon field with the created polygon', function () {
           expect(building.polygon).toBeDefined();
        });
        
        it('should prepare a control for changing floors', function () {
            expect(L.control.layers)
                .toHaveBeenCalledWith(null, null, {
                    collapsed: false
                });
        });
        
    });
    

    
    describe('when asked to add a floor', function () {
        var floorImgOv, floorTitle, fakeLayerGroup;
        
        beforeEach(function () {
            
            // Arrange
            floorTitle = 'Title';
            floorImgOv = L.imageOverlay();
            
            spyOn(building._floorLayerGroup, 'addLayer');
            
            fakeLayerGroup = L.layerGroup();
            spyOn(fakeLayerGroup, 'addLayer');
            spyOn(L, 'layerGroup').andReturn(fakeLayerGroup);
        });
        
        var act = function (addAsDefault) {
            building.addFloor(floorTitle, floorImgOv, addAsDefault);
        }
        
        describe('when the floor does not exist', function () {
            
            beforeEach(function () {
                
                spyOn(building._floorControl, 'addBaseLayer');
                // Expect there to be no floors defined.
                expect(Object.keys(building.floors).length).toBe(0);
                
                
            });
            
            describe('always', function () {
                
                beforeEach(function () {
                    act();
                });
                            
                it('should create a new layerGroup available under the floor name', function () {
                    expect(L.layerGroup).toHaveBeenCalledWith();
                    expect(building.floors[floorTitle]).toBe(fakeLayerGroup);
                });
                
                it('should register the layerGroup with the floorControl', function () {
                    
                    expect(building._floorControl.addBaseLayer)
                    .toHaveBeenCalledWith(fakeLayerGroup, floorTitle);
                });
                
            });
            
            describe('and the floor is indicated as default', function () {
               
                beforeEach(function () {
                   
                    act(true);  // Add as default
                });
                
                it('should make a reference to the layerGroup', function (){
                    
                   expect(building._defaultFloor).toBe(fakeLayerGroup);
                });
                
            });
            
            describe('and the floor is not indicated as default', function () {
                beforeEach(function () {
                    building._defaultFloor = "unchanged";
                    act(false); // Not as default
                });
                
                it('should not make a reference to the layerGroup', function () {
                    expect(building._defaultFloor).toBe("unchanged");
                });
            });
        });
        
        describe('and the floor already exists', function () {
            // Not implemented.
            
        });
        
        describe('always', function () {
           
            beforeEach(function () {
                
                act(); 
            });
        
            it('should add the image to the floor layer group', function () {
                
                expect(building.floors[floorTitle].addLayer)
                    .toHaveBeenCalledWith(floorImgOv)
            });
        });
        

        
    });
    
    describe('when asked to add to map', function () {
                
        beforeEach(function () {
            
            spyOn(building.polygon, 'addTo');
            
            building.addTo(map);
        });
        
        it('should add a polygon to the map which represents the building', function () {
            expect(building.polygon.addTo).toHaveBeenCalledWith(map);
        });
        
    });
    
    describe('when asked to focus', function() {
        var floor, fakeBounds;
        
        beforeEach(function () {
            
            // Assemble
            fakeBounds = "fakeBounds";
            spyOn(building.polygon, 'getBounds').andReturn(fakeBounds);
            
            // Make defualt floor stub
            building._defaultFloor = L.layerGroup()
            spyOn(building._defaultFloor, 'addTo');
            
            var fakeLayerGroup = L.layerGroup();
            spyOn(fakeLayerGroup, 'addTo');
            
            building.floors = { 'First': fakeLayerGroup };
            building.addTo(map);

            // Act
            building.focusOn();
        });
        
        it('should have the map focus on the building', function() {
            expect(map.fitBounds).toHaveBeenCalledWith(fakeBounds);
        });
        
        it('should provide a floor control on the map', function () {
            expect(map.addControl).toHaveBeenCalledWith(building._floorControl);
        });
        
        it('should display the first floor image overlay', function () {
            expect(building._defaultFloor.addTo).toHaveBeenCalledWith(map);
        });
        
        it('should show all of the points in the building', function () {
            // Not implemented
        });
        
    });
    
    describe('when asked to unfocus', function (){
        
        it('should remove all of the markers in the building', function () {
            
        });
        
        it('should restore the previous view', function () {
            
        });
        
    });
    
});